process Filter_ChrM {
  // Removes potential ChrM variant, as we dont know what these are called before they show up
  // echo true

  input:
  path inputfile

  output:
  path "${inputfile.getSimpleName()}.chrM-filtered.vcf", emit: vcf

  shell:
  '''
  # Naive grep to filter out potential ChrM variants if present (These can be included in the future once naming convention is sorted)
  cat !{inputfile} | grep -v 'gNomen=ChrM'> !{inputfile.getSimpleName()}.chrM-filtered.vcf
  '''
}
