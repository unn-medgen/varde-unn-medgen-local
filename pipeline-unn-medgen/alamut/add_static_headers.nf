process Add_Static_Headers {
  // Adds a static list of contig=ID to headers

  input:
  path inputfile

  output:
  path "${inputfile.getSimpleName()}.contig-ids.vcf", emit: vcf

  shell:
  '''
  # Extract ## headers. Change ID to uppercase
  # cat !{inputfile} | grep '^##' > headers
  # Extract #CHROM header
  cat !{inputfile} | grep '^#CHROM' > header
  # Extract variants
  cat !{inputfile} | grep -v '^#' > variants
  # Add static headers to ## headers
  cat << EOF > headers
  ##fileformat=VCFv4.4
  ##INFO=<ID=gNomen,Number=1,Type=String,Description="gDNA-level HGVS nomenclature">
  ##INFO=<ID=CLASS,Number=1,Type=String,Description="Variant classification">
  ##INFO=<ID=ACMG,Number=.,Type=String,Description="ACMG criteria">
  ##INFO=<ID=PUBMED,Number=.,Type=String,Description="List of Pubmed IDs">
  ##INFO=<ID=SVLEN,Number=.,Type=Integer,Description="Structural variant length">
  ##INFO=<ID=YEAR,Number=1,Type=Integer,Description="Year of interpretation">
  ##contig=<ID=1,length=249250621>
  ##contig=<ID=2,length=243199373>
  ##contig=<ID=3,length=198022430>
  ##contig=<ID=4,length=191154276>
  ##contig=<ID=5,length=180915260>
  ##contig=<ID=6,length=171115067>
  ##contig=<ID=7,length=159138663>
  ##contig=<ID=8,length=146364022>
  ##contig=<ID=9,length=141213431>
  ##contig=<ID=10,length=135534747>
  ##contig=<ID=11,length=135006516>
  ##contig=<ID=12,length=133851895>
  ##contig=<ID=13,length=115169878>
  ##contig=<ID=14,length=107349540>
  ##contig=<ID=15,length=102531392>
  ##contig=<ID=16,length=90354753>
  ##contig=<ID=17,length=81195210>
  ##contig=<ID=18,length=78077248>
  ##contig=<ID=19,length=59128983>
  ##contig=<ID=20,length=63025520>
  ##contig=<ID=21,length=48129895>
  ##contig=<ID=22,length=51304566>
  ##contig=<ID=X,length=155270560>
  ##contig=<ID=Y,length=59373566>
  ##contig=<ID=MT,length=16569>
  EOF
  # Concat ##headers, # header and variants
  cat headers header variants > !{inputfile.getSimpleName()}.contig-ids.vcf
  '''
}
