process Filter_ChrY {
  // Removes potential HG38 variants, which should not be processed
  // echo true

  input:
  path inputfile

  output:
  path "${inputfile.getSimpleName()}.chrY-filtered.vcf", emit: vcf

  shell:
  '''
  # Naive grep to filter out potential ChrY variants if present (These can be included in the future once naming convention (ChrX/ChrY - 23?) is sorted)
  cat !{inputfile} | grep -v 'gNomen=ChrY' > !{inputfile.getSimpleName()}.chrY-filtered.vcf
  '''
}
