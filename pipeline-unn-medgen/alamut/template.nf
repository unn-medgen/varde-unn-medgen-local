process Template {
  // This is still just a template ment for converting/manipulating filetypes if needed
  echo true

  input:
  path inputfile

  //output:
  path "outputfile.{vcf,tsv}", emit: vcf

  shell:
  '''
  echo "This is tool is not implemented yet"
  cp !{inputfile} !{inputfile.getSimpleName()}.varde-compliant.vcf
  '''
}
