process Convert_5ity {
  // Converts innate text classification to 1-5

  input:
  path inputfile

  output:
  path "${inputfile.getSimpleName()}.5ity-converted.vcf", emit: vcf

  shell:
  '''
  # Filter out everything but the set of strings conforming to 5-ity
  cat !{inputfile} | grep '^#' > headers
  # Change ID=class to ID=CLASS
  sed -i 's/ID=class/ID=CLASS/' headers
  cat !{inputfile} | awk '/class=Benign/ || /class=Likely Benign/ || /class=Uncertain Significance/ || /class=Likely Pathogenic/ || /class=Pathogenic/' > 5ity-set
  # Convert 5-ity text to 5-ity numbers
  cat 5ity-set | sed -e 's/Likely Benign/2/g' | sed -e 's/Benign/1/g' | sed -e 's/Uncertain Significance/3/g' | sed -e 's/Likely Pathogenic/4/g' | sed -e 's/Pathogenic/5/g' > converted
  # Change "class" in 5-ity lines to "CLASS"
  # (Nextflow doesnt like backslash without using dollar escapes, so we have to expand regexp to every occurence. Boo.)
  cat converted | sed 's/class=1/CLASS=1/g' | sed 's/class=2/CLASS=2/g' | sed 's/class=3/CLASS=3/g' | sed 's/class=4/CLASS=4/g' | sed 's/class=5/CLASS=5/g' > converted2
  cat headers converted2 > !{inputfile.getSimpleName()}.5ity-converted.vcf
  '''
}
