process Convert_From_Json {
  // This is still just a template ment for converting/manipulating filetypes if needed
  debug true

  input:
  path inputfile
  val script

  output:
  path "${inputfile.getSimpleName()}.converted.vcf", emit: vcf

  shell:
  '''
  # Convert Alissa JSON to vcf using python script
  python3 !{script} !{inputfile}
  mv converted.vcf !{inputfile.getSimpleName()}.converted.vcf
  '''
}
