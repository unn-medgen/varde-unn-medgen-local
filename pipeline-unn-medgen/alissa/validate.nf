process Validate {
  // Validates vcfs against hg19
  // Output from this process should always end in *.varde-compliant.vcf

  input:
  path inputfile
  path hg19

  output:
  path "${inputfile.getSimpleName()}.varde-compliant.vcf", emit: vcf

  shell:
  '''
  # Decompose variants
  vt decompose -s !{inputfile} -o !{inputfile.getSimpleName()}.decomposed.vcf
  # Normalize variants, send to standard out and remove duplicates. (-n to skip)
  vt normalize !{inputfile.getSimpleName()}.decomposed.vcf -r /varde/hg19/hg19.fna -n > !{inputfile.getSimpleName()}.normalized.vcf
  # Uniq to remove duplicates
  vt uniq !{inputfile.getSimpleName()}.normalized.vcf -o !{inputfile.getSimpleName()}.varde-compliant.vcf
  '''
}
