process Rename_Chr23 {
  // Renames "23" to "X". Needs to be applied after sort process, or X will be at start

  input:
  path inputfile

  output:
  path "${inputfile.getSimpleName()}.x-renamed.vcf", emit: vcf

  shell:
  '''
  # Sed to rename "23" to "X"
  cat !{inputfile} | sed -e 's/^23/X/g' > !{inputfile.getSimpleName()}.x-renamed.vcf
  '''
}
