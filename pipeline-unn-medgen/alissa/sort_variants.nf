process Sort_Variants {
  // Sorts variants in vcf file from 1-23

  input:
  path inputfile

  output:
  path "${inputfile.getSimpleName()}.sorted.vcf", emit: vcf

  shell:
  '''
  # Extract headers before sort
  cat !{inputfile} | grep '^#' > !{inputfile}.headers
  # Sort 1-22
  cat !{inputfile} | grep -v '^#' | grep -E '^[0-9]{1,2}' | sort -h -k1 -k2 > !{inputfile}.1-22
  # Sort X/Y
  cat !{inputfile} | grep -v '^#' | grep -E '^(X|Y)' | sort -h -k1 -k2 > !{inputfile}.x-y
  # Concat header and variants
  cat !{inputfile}.headers !{inputfile}.1-22 !{inputfile}.x-y > !{inputfile.getSimpleName()}.sorted.vcf
  '''
}
