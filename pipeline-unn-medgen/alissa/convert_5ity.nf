process Convert_5ity {
  // Converts innate text classification to 1-5

  input:
  path inputfile

  output:
  path "${inputfile.getSimpleName()}.5ity-converted.vcf", emit: vcf

  shell:
  '''
  # Separate headers and variants
  cat !{inputfile} | grep '^#' > headers
  cat !{inputfile} | grep -v '^#' > variants
  # Convert 5-ity text to 5-ity numbers
  cat variants | sed -e 's/CLASS=Likely Benign/CLASS=2/gi' | sed -e 's/CLASS=Benign/CLASS=1/gi' | sed -e 's/CLASS=VOUS/CLASS=3/gi' | sed -e 's/CLASS=Likely Pathogenic/CLASS=4/gi' | sed -e 's/CLASS=Pathogenic/CLASS=5/gi' > converted
  # Concatenate
  cat headers converted > !{inputfile.getSimpleName()}.5ity-converted.vcf
  '''
}
