// Include modules for each tool
include {Filter_HG38} from './alamut/filter_hg38.nf'
include {Filter_ChrY} from './alamut/filter_chrY.nf'
include {Filter_ChrM} from './alamut/filter_chrM.nf'
include {Filter_50} from './alamut/filter_50.nf'
include {Convert_5ity} from './alamut/convert_5ity.nf'
include {Sort_Variants} from './alamut/sort_variants.nf'
include {Rename_Chr23} from './alamut/rename_chr23.nf'
include {Add_Static_Headers} from './alamut/add_static_headers.nf'
include {Add_Year_Null} from './alamut/add_year.nf'
include {Bcftools_Normalize} from './alamut/bcftools_normalize.nf'
include {Validate} from './alamut/validate.nf'

// This current implementation takes only vcf files from Alamut as of now.

workflow UNN_MedGen_Alamut {
  take:
    inputfiles
    hg19

  main:
    // Use this template to add a stage
    // Template(inputfiles)

    // Filter_HG38 filters out any variants pertaining to the HG38 reference genome, as HG38 is not yet supported by this implementation
    Filter_HG38(inputfiles)

    // Filter out ChrY variants. We have none atm, but our vcf references 23, which is always ChrX... so far
    Filter_ChrY(Filter_HG38.out.vcf)

    // Filter out ChrM, as we have none atm, and we don't know what the variant will be called.
    Filter_ChrM(Filter_ChrY.out.vcf)

    // 5-ity conversion from text to numbers
    Convert_5ity(Filter_ChrM.out.vcf)

    // Sort from lowest to highest (chromosome)
    Sort_Variants(Convert_5ity.out.vcf)

    // Rename 23 variants to X (Needs to come after sort for)
    Rename_Chr23(Sort_Variants.out.vcf)

    // Add year 0 (This inforamtion is lacking for Alamut)
    // Add_Year_Null(Rename_Chr23.out.vcf)

    // Add headers
    Add_Static_Headers(Rename_Chr23.out.vcf)

    // Filter all variants longer than 50 bp
    Filter_50(Add_Static_Headers.out.vcf)

    // Remove inconsistencies and other trash with bcftools
    Bcftools_Normalize(Filter_50.out.vcf)

    // Validate validates the final VCF
    Validate(Bcftools_Normalize.out.vcf, hg19)

    // Output
    emit:
      vcf = Validate.out.vcf
      //log = Bcftools_Normalize.out.log
}
