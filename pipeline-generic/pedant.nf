process Pedant {
  // Debug true prints error from Pedant
  debug true

  input:
  path inputfile

  output:
  val true

  shell:
  '''
  petimeter validate --debug !{inputfile}
  '''
}
