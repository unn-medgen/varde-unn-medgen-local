process Bcftools_Merge {

  input:
  val pipeline
  path inputfiles
  path indexfiles

  output:
  path "*.vcf", emit: vcf
  path "*.log", emit: log

  shell:
  '''
  date_affix=$(date +"%Y%m%d")
  bcftools merge -m none !{inputfiles} -Oz -o !{pipeline}-tmp.gz
  bcftools index !{pipeline}-tmp.gz
  # Normalize, split multiallelic
  bcftools norm -a -Ob --check-ref wx -f /varde/hg19/hg19.fna -m- !{pipeline}-tmp.gz -o !{pipeline}-norm.bcf.gz --write-index 2&>> !{pipeline}-merge.log
  # Remove dups after merging
  bcftools norm -Ov -f /varde/hg19/hg19.fna !{pipeline}-norm.bcf.gz -o !{pipeline}-norm -d none 2&>> !{pipeline}-merge.log
  # Sort
  bcftools sort -Ov !{pipeline}-norm -o !{pipeline}-varde-\$date_affix.vcf 2&>> !{pipeline}-merge.log
  '''
}
