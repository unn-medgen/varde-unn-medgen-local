process Decrypt_Vcf {
  // If --decrypt is given as a parameter, inputfiles are decrypted
  // debug true

  input:
  val ready
  path inputfile
  val pass

  output:
  path "${inputfile.getSimpleName()}.decrypted.vcf", emit: decrypted

  shell:
  '''
  # Decrypt file using the imported and unlocked key
  gpg --batch --pinentry-mode=loopback --passphrase !{pass} -d !{inputfile} > !{inputfile.getSimpleName()}.decrypted.vcf 
  '''
}
