process Download_Input {
  // debug true

  input:
  path privkey
  val pipeline
  val dir

  output:
  path "input/*.vcf.gpg", optional: true, emit: vcf
  path "input/*.json.gpg", optional: true, emit: json

  shell:
  '''
  # Git clone input repository with valid private key
  GIT_SSH_COMMAND='ssh -i !{privkey} -o IdentitiesOnly=yes -o StrictHostKeyChecking=no' git clone git@gitlab.com:unn-medgen/varde-local-input.git
  mkdir input
  cp varde-local-input/input/!{pipeline}/!{dir}/* input/
  '''
}
