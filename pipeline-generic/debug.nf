process Debug {
  // If params.debug=True is set, this process writes unencrypted output to params.outputdir
  publishDir "output/", mode: 'copy'

  input:
  path inputfile

  output:
  path "${inputfile}.debug"

  shell:
  '''
  cp !{inputfile} !{inputfile}.debug
  '''
}
