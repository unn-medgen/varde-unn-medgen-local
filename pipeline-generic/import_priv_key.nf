process Import_Gpg_Priv_Key {
  // If --decrypt is given as a parameter, inputfiles are decrypted
  // debug true

  input:
  path privkey
  val pass

  output:
  val true

  shell:
  '''
  # Import and unlock the private key
  gpg --batch --passphrase !{pass} --import !{privkey}
  '''
}
