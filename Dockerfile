# Based on Amazon 2, AWS (centos, rhel etc.)
FROM nextflow/nextflow:23.04.3
# Install git
RUN yum install -y git
# Install compilers and a bunch of other crap needed
RUN yum groupinstall -y "Development Tools"
RUN yum install -y bzip2-devel
RUN yum install -y xz-devel
RUN yum install -y libcurl-devel
RUN yum install -y openssl-devel
# gnupg2.x86_64 (2.0.22) is installed by default
# Install rest in /varde/software
WORKDIR /varde/software
# Install Validator, VT
RUN yum install -y git
RUN git clone https://github.com/atks/vt.git
WORKDIR /varde/software/vt
RUN git submodule update --init --recursive
RUN make
ENV PATH="$PATH:/varde/software/vt"
# Download hg19
WORKDIR /varde/hg19
RUN curl -o hg19.fna.gz https://ftp.ncbi.nlm.nih.gov/genomes/archive/old_genbank/Eukaryotes/vertebrates_mammals/Homo_sapiens/GRCh37.p13/seqs_for_alignment_pipelines/GCA_000001405.14_GRCh37.p13_no_alt_analysis_set.fna.gz
RUN curl -o README.txt https://ftp.ncbi.nlm.nih.gov/genomes/archive/old_genbank/Eukaryotes/vertebrates_mammals/Homo_sapiens/GRCh37.p13/seqs_for_alignment_pipelines/README_ANALYSIS_SETS
# (this should be defined in process/*.nf to save image space at some point. Maybe.)
RUN gzip -d hg19.fna.gz
# Store original headers just in case
RUN cat hg19.fna | grep -i '^>chr' > original_hg19_headers.txt
# Change contig headers to >1 instead of >chr1, including X, Y and M
RUN sed -i -E 's/^>chr([0-9XYM]+) .*$/>\1/' hg19.fna
# Maybe remove the rest of sequences ? Neccessary ?
# Install python3
RUN yum install -y python3
# gnupg2.x86_64 (2.0.22) is installed by default
# Download and build GnuPG v.2.4.3
WORKDIR /varde/software
RUN curl -o gnupg-2.4.3.tar.bz2 https://gnupg.org/ftp/gcrypt/gnupg/gnupg-2.4.3.tar.bz2
RUN tar -xvf gnupg-2.4.3.tar.bz2
# Download and unpack neccessary libs
WORKDIR /varde/software/gnupg-2.4.3
RUN curl -o libgpg-error-1.47.tar.bz2 https://gnupg.org/ftp/gcrypt/libgpg-error/libgpg-error-1.47.tar.bz2
RUN tar -xvf libgpg-error-1.47.tar.bz2
RUN curl -o libgcrypt-1.10.3.tar.bz2 https://gnupg.org/ftp/gcrypt/libgcrypt/libgcrypt-1.10.3.tar.bz2
RUN tar -xvf libgcrypt-1.10.3.tar.bz2
RUN curl -o libksba-1.6.5.tar.bz2 https://gnupg.org/ftp/gcrypt/libksba/libksba-1.6.5.tar.bz2
RUN tar -xvf libksba-1.6.5.tar.bz2
RUN curl -o libassuan-2.5.6.tar.bz2 https://gnupg.org/ftp/gcrypt/libassuan/libassuan-2.5.6.tar.bz2
RUN tar -xvf libassuan-2.5.6.tar.bz2
RUN curl -o npth-1.6.tar.bz2 https://gnupg.org/ftp/gcrypt/npth/npth-1.6.tar.bz2
RUN tar -xvf npth-1.6.tar.bz2
# Configure, make, make install
WORKDIR /varde/software/gnupg-2.4.3/libgpg-error-1.47
RUN ./configure && make && make install
WORKDIR /varde/software/gnupg-2.4.3/libgcrypt-1.10.3
RUN ./configure && make && make install
WORKDIR /varde/software/gnupg-2.4.3/libksba-1.6.5
RUN ./configure && make && make install
WORKDIR /varde/software/gnupg-2.4.3/libassuan-2.5.6
RUN ./configure && make && make install
WORKDIR /varde/software/gnupg-2.4.3/npth-1.6
RUN ./configure && make && make install
WORKDIR /varde/software/gnupg-2.4.3
RUN ./configure && make && make install
ENV PATH="$PATH:/varde/software/gnupg-2.4.3"
ENV LD_LIBRARY_PATH=/usr/local/lib:$LD_LIBRARY_PATH
# Hack: Restart gpg-agent to force creation of $HOME/.gnupg/* then change to pinentry-curses for no tty pinentry
# Also needs --pinentry-mode=loopback on decrypt
RUN gpg-agent --pinentry-program=/usr/bin/pinentry-curses --daemon
RUN echo "pinentry-program /usr/bin/pinentry-curses" > $HOME/.gnupg/gpg-agent.conf
# Install Samtools (For BCFTools merge)
WORKDIR /varde/software
RUN git clone --recurse-submodules https://github.com/samtools/htslib.git
RUN git clone https://github.com/samtools/bcftools.git
WORKDIR /varde/software/bcftools
RUN make && mkdir bin && mv bcftools bin/ 
ENV PATH="$PATH:/varde/software/bcftools/bin"
# Install Pedant
WORKDIR /varde/software/pedant
RUN curl -L -o pedant_Linux_x86_64.tar.gz https://gitlab.com/DPIPE/datasharing/varde/pedant/-/releases/v1.0.0/downloads/pedant_Linux_x86_64.tar.gz
RUN tar -zxvf pedant_Linux_x86_64.tar.gz
ENV PATH="$PATH:/varde/software/pedant"
# Install pycurl need to access VEP api
RUN yum install -y python3-devel
RUN pip3 install --no-input pycurl
# Preflight, copy repo
WORKDIR /varde
RUN mkdir input
Run mkdir output
COPY . .
